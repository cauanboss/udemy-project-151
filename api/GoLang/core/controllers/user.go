package controllers

import (
	"api-test/core/models"
	"context"
	"encoding/json"
	"fmt"
	"net/http"

	"github.com/gorilla/mux"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/readpref"
	"gopkg.in/mgo.v2/bson"
)

type UserController struct {
	client *mongo.Client
}

func NewUserController(s *mongo.Client) *UserController {
	return &UserController{s}
}

func (uc *UserController) GetUser(w http.ResponseWriter, r *http.Request) {

	ctx := context.TODO()

	name := mux.Vars(r)["name"]

	filter := bson.M{"name": name}

	u := models.User{}

	uc.client.Database("Udemy").Collection("users").FindOne(ctx, filter).Decode(&u)

	if err := uc.client.Ping(ctx, readpref.Primary()); err != nil {
		fmt.Println("Erroooooo")
	}
	w.Header().Set("content-type", "application/json")
	json.NewEncoder(w).Encode(u)
}

func (uc *UserController) CreateUser(w http.ResponseWriter, r *http.Request) {
	u := models.User{}

	json.NewDecoder(r.Body).Decode(&u)

	fmt.Println(u)

	uc.client.Database("Udemy").Collection("users").InsertOne(context.TODO(), u)

	s, err := json.Marshal(u)
	if err != nil {
		fmt.Println(err)
	}
	fmt.Println(string(s))

	w.Header().Set("content-type", "application/json")
	json.NewEncoder(w).Encode(u)
}

func (uc *UserController) DeleteUser(w http.ResponseWriter, r *http.Request) {
	u := models.User{}

	id := mux.Vars(r)["id"]

	filter := bson.ObjectIdHex(id)

	uc.client.Database("Udemy").Collection("users").FindOneAndDelete(context.TODO(), filter)

	s, err := json.Marshal(u)
	if err != nil {
		fmt.Println(err)
	}
	w.Header().Set("content-type", "application/json")
	json.NewEncoder(w).Encode(string(s))
}
